;;; keybindings/+elfeed.el -*- lexical-binding: t; -*

;;an Emacs Atom/RSS feed reader.

(setq rmh-elfeed-org-files (list "~/my/elfeed.org"))

(map!
	:leader
	:prefix "a"
	(:desc "Open elfeed" "f" #'emms)

)