;;; keybindings/+centaur.el -*- lexical-binding: t; -*

;; Displays tabs at the top of the window similar to tabbed web browsers

;; To use tabs in Doom Emacs, be sure to uncomment "tabs" in Doom's ~init.el~.
;; Displays tabs at the top of the window similar to tabbed web browsers such as Firefox.
;; The default binding for ~centaur-tabs-forward~ and ~centaur-tabs-backward~ are ~g t~ and ~g T~ respectively.

;; | COMMAND                     | DESCRIPTION          | KEYBINDING       |
;; | --------------------------- | -------------------- | ---------------- |
;; | centaur-tabs-local-mode     | /Toggle tabs on/off/ | SPC t c          |
;; | centaur-tabs-forward        | /Next tab/           | g <right> or g t |
;; | centaur-tabs-backward       | /Previous tab/       | g <left> or g T  |
;; | centaur-tabs-forward-group  | /Next tab group/     | g <down>         |
;; | centaur-tabs-backward-group | /Previous tab group/ | g <up>           |
;; | --------------------------- | -------------------- | ---------------- |


(setq
	centaur-tabs-set-bar 'over
	centaur-tabs-set-icons t
	centaur-tabs-gray-out-icons 'buffer
	centaur-tabs-height 24
	centaur-tabs-set-modified-marker t
	centaur-tabs-style "bar"
	centaur-tabs-modified-marker "•"
)
(map!
	:leader
    :desc "Toggle tabs on/off" "t t" #'centaur-tabs-local-mode
)
(evil-define-key 'normal centaur-tabs-mode-map
	(kbd "g <right>") 'centaur-tabs-forward ; default Doom binding is 'g t'
	(kbd "g <left>")  'centaur-tabs-backward ; default Doom binding is 'g T'
	(kbd "g <down>")  'centaur-tabs-forward-group
	(kbd "g <up>")    'centaur-tabs-backward-group
)
