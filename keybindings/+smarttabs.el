;;; keybindings/+smarttabs.el -*- lexical-binding: t; -*

(setq-default indent-tabs-mode nil)

(add-hook 
  'c-mode-common-hook
  (lambda () 
    (setq indent-tabs-mode t)
  )
)